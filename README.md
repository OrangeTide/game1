# Game I

## Introduction

An example game I wrote one evening.

Early screenshot:
![Game Over](docs/images/gameover.png)

### Supported Platforms

* Linux

## Building, Installation and Prerequisites

### Prerequisite: Raylib

Prerequisites (Ubuntu or Debian):
```sh
sudo apt install libasound2-dev mesa-common-dev libx11-dev libxrandr-dev libxi-dev xorg-dev libgl1-mesa-dev libglu1-mesa-dev
```

Download & Build:
```sh
git clone --depth=2 https://github.com/raysan5/raylib.git raylib
cd raylib/src/
make PLATFORM=PLATFORM_DESKTOP CFLAGS="-flto -Os" # To make the static version.
# make PLATFORM=PLATFORM_DESKTOP RAYLIB_LIBTYPE=SHARED # To make the dynamic shared version.
```

Install System-wide:
```sh
sudo make install # Static version.
# sudo make install RAYLIB_LIBTYPE=SHARED # Dynamic shared version.
```

If you aren't happy:
```sh
sudo make uninstall
sudo make uninstall RAYLIB_LIBTYPE=SHARED
```

*TODO* update documentation for using CMake

### Building

Linux:
```sh
make
```

## Running

Linux:
```sh
./game1
```

## License

Pick any one of these:

* No license / PUBLIC DOMAIN
* [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
* [UNLICENSE](https://unlicense.org/UNLICENSE)
* [WTFPL](http://www.wtfpl.net/txt/copying/)
* [0BSD](https://opensource.org/licenses/0BSD)
