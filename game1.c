/* game1.c : example game using Raylib
 * PUBLIC DOMAIN, CC0, WTFPL, or UNLICENSE. (your choice)
 */
#include <stdbool.h>
#include <raylib.h>

/* Settings: you can change these */
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define VIRTUAL_WIDTH 320
#define VIRTUAL_HEIGHT 240

#if defined(PLATFORM_DESKTOP)
#define GLSL_VERSION	330
#else
#define GLSL_VERSION	100
#endif

static struct {
	short width, height;
	short virtual_width, virtual_height;
	RenderTexture2D target;
	float render_size[2];
	bool scanlines_enabled;
} screen = { SCREEN_WIDTH, SCREEN_HEIGHT };

enum game_state {
	GS_MAIN_MENU,
	GS_PLAY_GAME,
	GS_GAME_OVER,
};

static struct {
	enum game_state state;
} model = { GS_MAIN_MENU, };

static struct {
	Font title_font; /* font for title screens */
	Shader post_shader; /* post-processing shader */
	int post_shader_render_size_loc; /* location of "renderSize" */
	int post_shader_scanlines_enabled_loc; /* location of "scanlinesEnabled" */
} assets;

static const char title_msg[] = "Title Screen";
static const char gameover_msg[] = "Game Over !";
static bool game_done = false;

static void
draw_center(Font *fontptr, const char *msg, float fontSize, float spacing)
{
	Vector2 pos = MeasureTextEx(*fontptr, msg, fontSize, spacing);

	pos.x = (screen.virtual_width - pos.x) / 2;
	pos.y = (screen.virtual_height - pos.y) / 2;
	DrawTextEx(assets.title_font, msg, pos, assets.title_font.baseSize, 2, WHITE);
}

static void
set_scanlines_enabled(bool v)
{
	screen.scanlines_enabled = v;
	SetShaderValue(assets.post_shader, assets.post_shader_scanlines_enabled_loc, &screen.scanlines_enabled, UNIFORM_INT);
}

/* draws scene to a small texture */
static void
prepaint(void)
{
	BeginTextureMode(screen.target);
	ClearBackground(BLACK);

	switch (model.state) {
	case GS_MAIN_MENU:
		draw_center(&assets.title_font, title_msg, assets.title_font.baseSize, 2);
		break;
	case GS_GAME_OVER:
		draw_center(&assets.title_font, gameover_msg, assets.title_font.baseSize, 2);
		break;
	}
	EndTextureMode();
}

static void
paint(void)
{
	prepaint();

	BeginDrawing();
	ClearBackground(BLUE);

	////////////////////////////////////////////////////////////////////////

	BeginShaderMode(assets.post_shader);

	DrawTexturePro(screen.target.texture,
		(Rectangle){0, 0, screen.virtual_width, -screen.virtual_height},
		(Rectangle){0, 0, screen.width, screen.height},
		(Vector2){ 0, 0 }, 0.f, WHITE);

	EndShaderMode();

	////////////////////////////////////////////////////////////////////////

	EndDrawing();
}

static void
model_update(void)
{
	if (WindowShouldClose())
		game_done = true;

	if (IsKeyPressed(KEY_R))
		set_scanlines_enabled(!screen.scanlines_enabled);

	switch (model.state) {
	case GS_MAIN_MENU:
		if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) || IsKeyPressed(KEY_ENTER))
			model.state = GS_PLAY_GAME;
		break;
	case GS_PLAY_GAME:
		// TODO: if dead ...
			model.state = GS_GAME_OVER;
		break;
	case GS_GAME_OVER:
		if (IsKeyPressed(KEY_Q) || IsKeyPressed(KEY_X))
			game_done = true;
		else if (IsKeyPressed(KEY_ENTER) || IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
			model.state = GS_MAIN_MENU;
		break;
	}
}

static void
load_assets(void)
{
	assets.title_font = LoadFontEx("assets/title.ttf", 32, 0, 250);

	assets.post_shader = LoadShader(0, TextFormat("assets/shaders/glsl%i/raster.fs", GLSL_VERSION));

	/* set the vec2 renderSize in the shader */
	assets.post_shader_render_size_loc = GetShaderLocation(assets.post_shader, "renderSize");
	SetShaderValue(assets.post_shader, assets.post_shader_render_size_loc, screen.render_size, UNIFORM_VEC2);
	assets.post_shader_scanlines_enabled_loc = GetShaderLocation(assets.post_shader, "scanlinesEnabled");
	set_scanlines_enabled(true);
}

static void
init(void)
{
	InitWindow(screen.width, screen.height, "Game I");

	SetTargetFPS(60);

	screen.render_size[0] = screen.width;
	screen.render_size[1] = screen.height;
	screen.virtual_width = VIRTUAL_WIDTH;
	screen.virtual_height = VIRTUAL_HEIGHT;
	screen.target = LoadRenderTexture(screen.virtual_width, screen.virtual_height);

	load_assets();
}

static void
done(void)
{
	UnloadRenderTexture(screen.target);
	UnloadFont(assets.title_font);
	CloseWindow();
}

int
main(int argc, char *argv[])
{
	init();

	while (!game_done) {
		model_update();
		paint();
	}

	done();

	return 0;
}
