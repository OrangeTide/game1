#version 100

precision mediump float;

// Input vertex attributes (from vertex shader)
varying vec2 fragTexCoord;
varying vec4 fragColor;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;

// NOTE: Add here your custom variables

uniform bool scanlinesEnabled = true;
uniform vec2 renderSize; // required!
float offset = 0.0;

uniform float time;

void main()
{
   vec4 color = texture2D(texture0, fragTexCoord);

    if (scanlinesEnabled) {
        float frequency = renderSize.y / 3.0;
        float globalPos = (fragTexCoord.y + offset) * frequency;
        float wavePos = cos((fract(globalPos) - 0.5) * 3.141592);

        gl_FragColor = mix(vec4(0.0, 0.3, 0.0, 0.0), color, wavePos);
    } else {
        gl_FragColor = color;
    }
}
