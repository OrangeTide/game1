RAYLIB_LDLIBS := -lraylib $(shell pkg-config --libs gl x11) -ldl -lrt
RAYLIB_CFLAGS := $(shell pkg-config --cflags gl x11) -DPLATFORM_DESKTOP
all :: game1
clean :: ; $(RM) game1
.PHONY : all clean
game1 : game1.c
game1 : CFLAGS = -flto -pthread $(RAYLIB_CFLAGS)
# game1 : CPPFLAGS = -I/usr/local/include
game1 : LDLIBS = $(RAYLIB_LDLIBS) -lm
